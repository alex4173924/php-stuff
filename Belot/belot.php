<?php

$my_card_deck = [ 0 => "2C", 1 => "3C", 2 => "4C", 3 => "5C", 4 => "6C", 5 => "7C", 6 => "8C", 
                  7 => "9C", 8 => "10C", 9 => "11C", 10 => "12C", 11 => "13C", 12 => "14C",
                  13 => "2D", 14 => "3D", 15 => "4D", 16 => "5D", 17 => "6D", 18 => "7D", 19 => "8D",
                  20 => "9D", 21 => "10D", 22 => "11D", 23 => "12D", 24 => "13D", 25 => "14D",
                  26 => "2H", 27 => "3H", 28 => "4H", 29 => "5H", 30 => "6H", 31 => "7H", 32 => "8H",
                  33 => "9H", 34 => "10H", 35 => "11H", 36 => "12H", 37 => "13H", 38 => "14H",
                  39 => "2S", 40 => "3S", 41 => "4S", 42 => "5S", 43 => "6S", 44 => "7S", 45 => "8S",
                  46 => "9S", 47 => "10S", 48 => "11S", 49 => "12S", 50 => "13S", 51 => "14S"
];



$my_play_deck = [];

foreach($my_card_deck as $card_key => $card_value) {

    if($card_value[0] > '6' || $card_value[0] == '1' ) {
        $my_play_deck[$card_key] = $card_value;
    }
}


shuffle_play_deck($my_play_deck);


function shuffle_play_deck(&$arr_deck) {

    $temp_arr = [];
    $card_keys = array_keys($arr_deck);

    shuffle($card_keys);

    foreach($card_keys as $key) {
        
        $temp_arr[$key] = $arr_deck[$key];
        unset($arr_deck[$key]);

    }

    $arr_deck = $temp_arr;
}


$players = [
    $rumqna = [],
    $vasil = [],
    $stoqnka = [],
    $krasi = []
];



echo "<br><br>";

deal_to_players($my_play_deck);
show_score($players, count($players[0]));



function deal_to_players($deck) {


    global $players;

    $counter = 0;


    $pl_names = ["Rumqna", "Vasil", "Stoqnka", "Krasi"];


    $three_card_deal = 3;
    $two_card_deal = 2;
    
    $size_pl = count($players);

    $deck_vals_arr = array_values($deck);



    $dealing = [3, 2, 3];


    foreach($dealing as $deal) {


        for($i = 0; $i < $size_pl; $i++) {
            
            for($j = 0; $j < $deal; $j++) {
    
                array_push($players[$i], array_shift($deck_vals_arr));         
    
            }

        }

    }    


    for($i = 0; $i < $size_pl; $i++) {

        echo "<h4>Name: " . $pl_names[$i] . "</h4><br>";

        for($j = 0; $j < count($players[0]); $j++) {
            
            echo $players[$i][$j] . " ";

        }


        echo "<br><br>";
    }

}



function show_score($players, $size_p) {

    $size_cards = $players[0];

    $current_hand_expl = '';

    
    $temp_hand = [
        $rumqna_h = [],
        $vasil_h = [],
        $stoqnka_h = [],
        $krasi_h = []
    ];


    for($i = 0; $i < $size_p; $i++) {

        for($j = 0; $j < $size_cards; $j++) {

            $current_hand_expl = preg_split('/(\d+)/', $players[$i][$j]);

            $temp_hand[$i][$j] = $current_hand_expl[0];
            $temp_hand[$current_hand_expl[0]] = $current_hand_expl[1];

        }
    }

    foreach($temp_hand as $pl => $card) {
        echo '<h2>' . $pl . ' ' . $card . '</h2>';
        echo "<br/>";
    }

}


?>
