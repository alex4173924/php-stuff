<form action="" method="post">
    <label for="name">Check your numbers:</label><br/>
    <input type="text" name="number1" id="name1" ><br/>
    <input type="submit" name="submit">
</form>

<?php
 
if($_SERVER['REQUEST_METHOD'] === 'POST'){
    if(isset($_POST['submit'])){
        $num = $_POST['number1'];
        $numString = strval($num);
        $numLength = strlen($numString);
        
        $regExGroups = [];
        
        function getRepeatForIndex($index, $isLast) {
            if($index === 0) return "";
            if($index === 1) return "[0-9]";
            $max = $isLast;
            return "{".$index."}$|^"."[0-9]";
        }
        
        function getGroupForIndex($numString, $index, $revIndex) {
            $value = intval($numString[$revIndex]);
            $nextValue = $index === 0 ? $value : $value - 1;
            $group = $nextValue === 0 ? "[0-9]" : "[0-".$nextValue."]";
            $isLastIndex = $index === strlen($numString);
            $groupRepeat = getRepeatForIndex($index, $isLastIndex)."$";
            
            return $group.$groupRepeat;
        }
        
        for($index=0; $index<=$numLength-1; $index++){
            $revIndex = ($numLength-1)-$index;
            $group = getGroupForIndex($numString, $index, $revIndex);
            $regExGroups[] ="^". substr_replace($numString, $group, $revIndex, $index+1);
        }
        
        echo implode("|",$regExGroups);
    }
}
