<?php

show_html();
 
if($_SERVER['REQUEST_METHOD'] === 'POST'){

    if(isset($_POST['submit'])) {

        $num = $_POST['number1'];
        $numString = strval($num);
        $numLength = strlen($numString);
        
        $regExGroups = [];
        
        if(isset($_POST['True']) && !isset($_POST['False'])) {

            function getRepeatForIndex($index, $isLast) {
                if($index === 0) return "";
                if($index === 1) return "[0-9]";
                
                $min = $index;
                $max = $isLast ? "," : "";
                
                return "[0-9]{".$min.$max."}";
            }
            
            function getGroupForIndex($numString, $index, $revIndex) {
                $value = intval($numString[$revIndex]);
                $nextValue = $index === 0 ? $value : $value + 1;
                $group = $nextValue === 9 ? "[9]" : "[".$nextValue."-9]";
                $isLastIndex = $index+1 === strlen($numString);
                $groupRepeat = getRepeatForIndex($index, $isLastIndex);
                
                return $group.$groupRepeat;
            }
            
            for($index=0; $index<=$numLength-1; $index++){
                $revIndex = ($numLength-1)-$index;
                $group = getGroupForIndex($numString, $index, $revIndex);
                $regExGroups[] = substr_replace($numString, $group, $revIndex, $index+1);
            }
            
            echo implode("|",$regExGroups);

        } elseif(isset($_POST['False']) && !isset($_POST['True'])) {

            function getRepeatForIndex($index, $isLast) {
                if($index === 0) return "";
                if($index === 1) return "[0-9]";
                
                $min = $index;
                $max = $isLast ? "," : "";
                
                return "[0-9]{".$min.$max."}";
            }
            
            function getGroupForIndex($numString, $index, $revIndex) {
                $value = intval($numString[$revIndex]);
                $nextValue = $index === 1 ? $value : $value + 1;
                $group = $nextValue === 9 ? "[9]" : "[".$nextValue."-9]";
                $isLastIndex = $index+1 === strlen($numString) - 1;
                $groupRepeat = getRepeatForIndex($index, $isLastIndex);
                
                return $group.$groupRepeat;
            }
            
            for($index=0; $index<=$numLength-2; $index++){
                $revIndex = ($numLength-1)-$index;
                $group = getGroupForIndex($numString, $index, $revIndex);
                $regExGroups[] = substr_replace($numString, $group, $revIndex, $index+1);
            }
            
            echo implode("|",$regExGroups);

        } else {
            header('location: task_4.php');
        }
    }
}



function show_html() {
    echo <<<HTML
    <form action="" method="post">
    <label for="name">Check your numbers:</label><br/>
    <input type="text" name="number1" id="name1" pattern="/^{[0-9][0-9][0-9]}$/m"><br/>
    <input type="checkbox" name="True">With limit values<br/>
    <input type="checkbox" name="False">Without limit values<br/>
    <input type="submit" name="submit">
    </form>
    HTML;
}
